#!/usr/bin/env nix-shell
#!nix-shell -i bash --pure --option sandbox false

jupyter lab --allow-root --no-browser --ip=0.0.0.0 --port=8888 --LabApp.token=''
